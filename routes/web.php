<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/login', 'LoginController@getLogin')->middleware('guest');
Route::post('/login', 'LoginController@postLogin');
Route::get('/logout', 'LoginController@logout')->name('logout');


// Route::get('/admin', function() {
//     return view ('admin');
// })->middleware('auth:admin');

// Route::middleware('auth:admin')->group(function(){
//     Route::get('kelassiswa', 'AdminController@kelasSiswa');
//     Route::get('kelas1', 'AdminController@kelas1')->name('kelas1');
//     Route::get('kelas2', 'AdminController@kelas2')->name('kelas2');
//     Route::get('kelas3', 'AdminController@kelas3')->name('kelas3');
//     Route::get('kelas4', 'AdminController@kelas4')->name('kelas4');
//     Route::get('kelas5', 'AdminController@kelas5')->name('kelas5');
// });

Route::group(['prefix' =>'admin', 'middleware' => 'auth:admin'],  function() {
    Route::get('/', function() {
        return view('admin');
    });
    Route::get('kelassiswa', 'AdminController@kelasSiswa')->name('kelassiswa');
    Route::get('kelas1', 'AdminController@kelas1')->name('kelas1');
    Route::get('kelas2', 'AdminController@kelas2')->name('kelas2');
    Route::get('kelas3', 'AdminController@kelas3')->name('kelas3');
    Route::get('kelas4', 'AdminController@kelas4')->name('kelas4');
    Route::get('kelas5', 'AdminController@kelas5')->name('kelas5');
    Route::put('updateAll', 'AdminController@UpdateAll')->name('updateall');

   
    Route::get('input_siswa', 'AdminController@CreateSiswa')->name('createsiswa');
    Route::post('siswa_store', 'AdminController@SiswaStore')->name('siswa.store');
    Route::get('siswa_edit/{id}', 'AdminController@SiswaEdit')->name('siswa.edit');
    Route::put('siswa_update/{id}', 'AdminController@SiswaUpdate')->name('siswa.update');
    Route::delete('siswa_delete/{id}', 'AdminController@SiswaDelete')->name('siswa.delete');





    Route::get('input_guru', 'AdminController@InputGuru')->name('createguru');
    Route::post('input_store', 'AdminController@GuruStore')->name('guru.store');


    Route::get('input_orangtua', 'AdminController@InputOrangTua')->name('createorangtua');
    Route::post('input_store', 'AdminController@OrangTuaStore')->name('orangtua.store');

});

// Route::get('/guru', function() {
//     return view('guru');
// })->middleware('auth:guru');
Route::group(['prefix' => 'guru', 'middleware' => 'auth:guru'], function() {
    Route::get('/', function() {
        return view('guru');
    });
});

Route::get('/siswa', function() {
    return view('siswa');
})->middleware('auth:siswa');

Route::get('/orangtua', function() {
    return view('orangtua');
})->middleware('auth:orangtua');

Route::group(['prefix' => 'orangtua', 'middleware' => 'auth:orangtua'], function(){
    Route::get('/', function() {
        return view('orangtua');
    });

    Route::get('has/{id}', 'OrangTuaController@has');
});






