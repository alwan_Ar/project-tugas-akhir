@extends('layouts.global')


@section('content')
    <div class="col-md-8">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
    <form class="bg-white shadow-sm p-3" action="{{route('siswa.update', ['id'=>$siswa->id])}}" method="POST">
        @csrf
        <input type="hidden" value="PUT" name="_method">

        <label for="name">Nama</label>
        <input 
            class="form-control" 
            value="{{$siswa->name}}" 
            type="text" 
            name="name" 
            id="name"/>
        <br>

        <label for="email">email</label>
        <input 
            class="form-control" 
            value="{{$siswa->email}}" 
            type="email" 
            name="email" 
            id="email"/>
        <br>

        <label for="alamat">alamat</label>
        <textarea name="alamat" id="" cols="30" rows="10" class="form-control">{{$siswa->alamat}}</textarea>

      

        <input type="submit" value="Ubah" name="simpan" class="btn btn-primary" >
        </form>
    </div>
@endsection