@extends('layouts.global')

@section('content')
 <div class="col-md-8">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
   <form action="{{route('siswa.store')}}" method="post" class="bg-white shadow-sm p-3">
        @csrf

        <label for="name">Name</label>
        <input 
            class="form-control" 
            placeholder="masukan nama" 
            type="text" 
            name="name" 
            id="name"/>
        <br>

        <label for="email">email</label>
        <input 
            class="form-control" 
            placeholder="masukan email" 
            type="email" 
            name="email" 
            id="email"/>
        <br>

        <label for="password">password</label>
        <input 
            class="form-control" 
            placeholder="masukan password" 
            type="password" 
            name="password" 
            id="password"/>
        <br>

        <label for="alamat">alamat</label>
        <textarea 
            name="alamat" 
            id="alamat" 
            class="form-control"></textarea>

        <br>

        

        <input type="submit" value="simpan" name="simpan">
   </form>
 </div>
@endsection