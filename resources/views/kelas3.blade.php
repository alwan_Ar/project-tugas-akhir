{{-- 
<form action="{{route('updateall')}}" method="post">
    @foreach ($kelas as $data)
      @csrf
       <input type="hidden" name="_method" value="PUT">
        {{$data->name}}
        alamat
        {{$data->alamat}}
        <input type="checkbox" name="ids[]" id="ids[]" value="{{$data->id}}"><br>
    @endforeach
    
    <input type="submit" value="Naik Kelas">
    </form> --}}

    @extends('layouts.global')

    @section('content')
    <div class="col-md-8">
        @if (session('status'))
          <div class="alert alert-success">
              {{ session('status') }}
          </div>
        @endif
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Check</th>
              <th>Nama</th>
              <th>Alamat</th>
              <th>aksi</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($kelas as $data)
            <tr>
                <td>
                  <form 
                  enctype="multipart/form-data" 
                  action="{{route('updateall')}}"
                  method="POST">
                  @csrf
                  <input type="hidden" name="_method" value="PUT">
                  <input 
                  type="checkbox" 
                  name="ids[]"  
                  value="{{$data->id}}"
                   id="ids{{$loop->iteration}}"> 
                  <label for="ids{{$loop->iteration}}"></label>
                </td>
                <td>
                  {{$data->name}}
                </td>
                <td>
                  {{$data->alamat}}
                </td>
                <td>
                  <a href="" class="btn btn-primary">Edit</a>
                </td>
              </tr>
              @endforeach

          </tbody>
        </table>
        
          <input 
          class="btn btn-primary" 
          type="submit" 
          value="Naik Kelas"/>
          </form>
                
      </div>
    
   
    @endsection