
{{-- <form action="{{route('updateall')}}" method="post">
@foreach ($kelas as $data)
  @csrf
   <input type="hidden" name="_method" value="PUT">
    {{$data->name}}
    alamat
    {{$data->alamat}}
    <input type="checkbox" name="ids[]" id="ids[]" value="{{$data->id}}"><br>
@endforeach

<input type="submit" value="Naik Kelas">
</form> --}}


@extends("layouts.global")

@section("title") Kelas 2 @endsection

@section("content")

  {{-- <div class="col-md-8">

    <form 
        enctype="multipart/form-data" 
        class="bg-white shadow-sm p-3" 
        action="{{route('updateall')}}"
        method="POST">
        @csrf
        <input type="hidden" name="_method" value="PUT">
      @foreach ($kelas as $data)
        <input 
        type="checkbox" 
        name="ids[]"  
        value="{{$data->id}}"
         id="ids{{$loop->iteration}}"> 
        <label for="ids{{$loop->iteration}}"></label>

        {{$data->name}}
        {{$data->alamat}}<br>

      @endforeach
      
      


     
      

      <input 
        class="btn btn-primary" 
        type="submit" 
        value="Naik Kelas"/>
    </form>
  </div> --}}

  <div class="col-md-8">
    <table class="table table-striped">
      <thead>
        <tr>
          <th>Check</th>
          <th>Nama</th>
          <th>Alamat</th>
          <th>aksi</th>
        </tr>
      </thead>
      <tbody>
          <form 
          enctype="multipart/form-data" 
          action="{{route('updateall')}}"
          method="POST">
          <input 
          class="btn btn-primary" 
          type="submit" 
          value="Naik Kelas"/>
        @foreach ($kelas as $data)
        <tr>
            <td>
              
              @csrf
              <input type="hidden" name="_method" value="PUT">
              <input 
              type="checkbox" 
              name="ids[]"  
              value="{{$data->id}}"
               id="ids{{$loop->iteration}}"> 
              <label for="ids{{$loop->iteration}}"></label>
            </form>
            </td>
            <td>
              {{$data->name}}
            </td>
            <td>
              {{$data->alamat}}
            </td>
            <td>
              <a href="{{route('siswa.edit', ['id'=> $data->id])}}" class="btn btn-primary">Edit</a>
              <form 
                onsubmit="return confirm('Apakah Anda Yakin Ingin Menghapus?')" 
                class="d-inline" 
                action="{{route('siswa.delete', ['id' => $data->id ])}}" 
                method="POST">
              
                  @csrf
              
                  <input 
                    type="hidden" 
                    name="_method" 
                    value="DELETE">
              
                  <input 
                    type="submit" 
                    value="Delete" 
                    class="btn btn-danger btn-sm">
              </form>
            </td>
          </tr>
          @endforeach
      </tbody>
    </table>
   
    
  </div>

@endsection