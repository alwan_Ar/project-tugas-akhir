<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Siswa;
use App\Kelas;
use App\Guru;
use App\OrangTua;

class AdminController extends Controller
{
    //menampilkan kelas siswa
    public function kelasSiswa() {
       
        return view('kelassiswa');
    }

    public function kelas1() {
        $kelas  = Siswa::where('kelas_id', '=', '1')->get();
        return view('kelas1', ['kelas' => $kelas]);
    }

    public function kelas2() {
        $kelas  = Siswa::where('kelas_id', '=', '2')->get();
        return view('kelas2', ['kelas' => $kelas]);
    }

    public function kelas3() {
        $kelas  = Siswa::where('kelas_id', '=', '3')->get();
        return view('kelas3', ['kelas' => $kelas]);
    }
    public function kelas4() {
        $kelas  = Siswa::where('kelas_id', '=', '4')->get();
        return view('kelas4', ['kelas' => $kelas]);
    }
    
    public function kelas5() {
        $kelas  = Siswa::where('kelas_id', '=', '5')->get();
        return view('kelas5', ['kelas' => $kelas]);
    }

    public function UpdateAll(Request $request) {
        $ids = $request->ids;
        dd($ids);
        $siswa = Siswa::whereIn('id', $ids)->update(['kelas_id'=> DB::raw('kelas_id+1')]);
        return redirect()->back()->with('status', 'Proses Sukses');
    }

  
    public function CreateSiswa() {
        // dd(date('Y-m-d'));
    
                           
        return view('siswa.create');
    }

    public function SiswaStore(Request $request) {

        $siswa = new Siswa;
        $siswa->name = $request->name;
        $siswa->email = $request->email;
        $siswa->password = \Hash::make($request->password);
        $siswa->alamat = $request->alamat;
        $siswa->save(); 

        return redirect()->route('createsiswa')->with('status', 'Data Siswa Berhasil Ditambahkan');
    }

    public function SiswaEdit($id) {
        $siswa = Siswa::findOrFail($id);

        return view('siswa.edit', ['siswa' => $siswa]);
    }
    public function SiswaUpdate(Request $request, $id) {
        $siswa = Siswa::findOrFail($id);
        $siswa->name = $request->name;
        $siswa->email = $request->email;
        $siswa->alamat = $request->alamat;
        $siswa->save();

        return redirect()->route('kelassiswa')->with('status', 'Data Siswa Berhasil Di Ubah');
    }

    public function SiswaDelete($id) {
        $siswa = Siswa::where('id', $id)->delete();

        return redirect()->route('kelassiswa')->with('status', 'Data Siswa Berhasil Di Hapus');
    }

    public function InputGuru() {
        
        return view('guru.create');
    }

    public function GuruStore(Request $request) {
        $guru = new Guru;
        $guru->name = $request->name;
        $guru->email = $request->email;
        $guru->password = \Hash::make($request->password);
        $guru->save();

        return redirect()->route('createguru')->with('status', 'Data Siswa Berhasil Ditambahkan');
    }

    public function InputOrangTua() {
        $siswa = Siswa::
                    whereDate('created_at', date('Y-m-d'))
                    ->get();
        return view('orangtua.create', ['siswa' => $siswa]);
    }

    public function OrangTuaStore(Request $request) {
        $orangtua = new OrangTua;
        $orangtua->name = $request->name;
        $orangtua->email = $request->email;
        $orangtua->password = \Hash::make($request->password);
        $orangtua->alamat = $request->alamat;
        $orangtua->siswa_id = $request->wali_murid;
        $orangtua->save();

        return redirect()->route('createorangtua')->with('status', 'Data Orang TUa Berhasil Ditambahkan');
    }
}
