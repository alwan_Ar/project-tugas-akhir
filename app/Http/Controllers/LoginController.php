<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use App\Guru;
use Auth;


class LoginController extends Controller
{
    public function getLogin() {
        return view('login');
    }

    public function postLogin(Request $request) {
        $this->validate($request,[
            'email' => 'required|email',
            'password' => 'required'
        ]);


        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password])) {
            // if successful, then redirect to their intended location
          return redirect()->intended('/admin');
        } else if (Auth::guard('guru')->attempt(['email' => $request->email, 'password' => $request->password])) {
          return redirect()->intended('/guru');
        } else if(Auth::guard('siswa')->attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect()->intended('/siswa');
        }else if(Auth::guard('orangtua')->attempt(['email' => $request->email, 'password' => $request->password])){
            return redirect()->intended('/orangtua');
        }
    
    }

    public function logout() {
        if(Auth::guard('admin')->check()) {
            Auth::guard('admin')->logout();
        }else if (Auth::guard('guru')->check()) {
            Auth::guard('guru')->logout();
        }else if(Auth::guard('siswa')->check()) {
            Auth::guard('siswa')->logout();
        }else if(AUth::guard('orangtua')->check()){
            Auth::guard('orangtua')->logout();
        }

        return redirect('/login');
    }
}
