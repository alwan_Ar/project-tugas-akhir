<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Guru extends Authenticatable
{
    
    use Notifiable;
    
    //merujuk pada table Guru
    protected $table = 'guru';

    //primary key
    protected $primaryKey = 'id';


    protected $fillable = [
        'name', 'email', 'password',
    ];
    protected $hidden = ['password'];
}
