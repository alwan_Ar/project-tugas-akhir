<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class OrangTua extends  Authenticatable
{
    protected $table = 'orangtua';

    protected $primaryKey = 'id';


    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = ['password'];

    public function students() {
        return $this->hasMany('App\Siswa');
    }

   

}
