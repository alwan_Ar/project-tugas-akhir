<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    protected $table = 'kelas';

    protected $primaryKey = 'id';
    protected $foreignKey = 'id';


    protected $fillable = [
        'kelas', 
    ];

    public function students() {
        return $this->hasMany('App\Siswa');
    }

    public function classes() {
        return $this->hasManyThrough('App\OrangTua', 'App\Siswa');
    }
}

