<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Siswa extends Authenticatable
{
    use Notifiable;


    protected $table = 'siswa';

    protected $primaryKey = 'id';


    protected $fillable = [
        'name', 'email', 'password', 'kelas_id', 'orangtua_id'
    ];

    protected $hidden = ['password'];

    public function class() {
        return $this->belongsTo('App\kelas');
    }

    public function parent() {
        return $this->belongsTo('App\OrangTua');
    }
}
