-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 23, 2019 at 07:22 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel_sd`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', '$2y$10$/YT99zw7jNX9hnqlxOHSI.gGdY6FadXWXjHFgjHZrbXAsQarw/vtu', '2019-04-22 23:34:32', '2019-04-22 23:34:32');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files` (
  `id` int(10) UNSIGNED NOT NULL,
  `judul` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kelas` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guru_id` int(11) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `judul`, `file`, `kelas`, `path`, `guru_id`, `created_at`, `updated_at`) VALUES
(1, 'belajar', 'gh.pdf', 'kelas 1', 'kelas1/gh.pdf', 10, '2019-08-23 00:09:24', '2019-08-23 00:09:24'),
(2, 'bahasa 1', 'gh.pdf', 'kelas 2', 'kelas2/gh.pdf', 11, '2019-08-25 18:43:42', '2019-08-25 18:43:42'),
(3, 'test', 'Dalam era sistem informasi.docx', 'kelas 1', 'kelas1/Dalam era sistem informasi.docx', 10, '2019-08-25 20:18:42', '2019-08-25 20:18:42');

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE `guru` (
  `id` int(10) UNSIGNED NOT NULL,
  `nip` char(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_telp` char(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`id`, `nip`, `name`, `alamat`, `password`, `no_telp`, `email`, `created_at`, `updated_at`) VALUES
(10, '7889999088777', 'guru', 'sleman', '$2y$10$IjqSbK/ZC3aivKv5Rs2C9OfYCsfGZkXW7CqL5RZEH4DUtVD/EboG6', '086677555', 'guru@gmail.com', '2019-08-23 00:08:32', '2019-08-23 00:08:32'),
(11, '199404058235890218', 'Sri Utami', 'bantul', '$2y$10$.58B3v8JItWcOt.B7dN3BOQgRWbw/mM4j6y7usrHaL9AimAeZTAQO', '08567554677', 'sriutami@gmail.com', '2019-08-25 10:41:29', '2019-08-25 18:43:04'),
(12, '199803049867763321', 'Farhan abidin', 'bantul', '$2y$10$Adhgsxk6reeQUUSFzZRZhezdZ.VE2MhSPEyrYK8X7HPbXMRwCRO1m', '08522547786', 'farhanabidin39@gmail.com', '2019-08-25 10:42:31', '2019-08-25 10:42:31'),
(16, '789966677777885899', 'nafian', 'bantul', '$2y$10$jYiBwUfIAYonv3rfu2rabu0.ahlgYhMkrEQB10h/Fm./hPokbnKoS', '08562256789', NULL, '2019-08-26 16:45:50', '2019-08-26 16:45:50');

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id` int(10) UNSIGNED NOT NULL,
  `kelas` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`id`, `kelas`, `created_at`, `updated_at`) VALUES
(1, 'kelas 1', '2019-04-22 17:00:00', '2019-04-22 17:00:00'),
(2, 'kelas 2', NULL, NULL),
(3, 'kelas 3', '2019-04-22 17:00:00', '2019-04-22 17:00:00'),
(4, 'kelas 4', NULL, NULL),
(5, 'kelas 5', '2019-07-01 14:34:08', '2019-07-01 14:34:08'),
(6, 'kelas 6', '2019-07-01 14:34:36', '2019-07-01 14:34:36'),
(7, 'lulus', '2019-07-01 14:34:36', '2019-07-01 14:34:36');

-- --------------------------------------------------------

--
-- Table structure for table `mapel`
--

CREATE TABLE `mapel` (
  `id` int(10) UNSIGNED NOT NULL,
  `mapel` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mapel`
--

INSERT INTO `mapel` (`id`, `mapel`, `created_at`, `updated_at`) VALUES
(6, 'Matematika', '2019-08-25 08:45:16', '2019-08-25 08:45:16'),
(7, 'Bahasa Indonesia', '2019-08-25 08:45:28', '2019-08-25 08:45:28'),
(8, 'IPA', '2019-08-25 08:45:37', '2019-08-25 08:45:37'),
(9, 'PKN', '2019-08-25 08:46:01', '2019-08-25 08:46:01'),
(10, 'IPS', '2019-08-25 08:46:16', '2019-08-25 08:46:16');

-- --------------------------------------------------------

--
-- Table structure for table `mapel_siswa`
--

CREATE TABLE `mapel_siswa` (
  `id` int(10) UNSIGNED NOT NULL,
  `kd1` int(11) UNSIGNED DEFAULT NULL,
  `kd2` int(11) UNSIGNED DEFAULT NULL,
  `kd3` int(11) UNSIGNED DEFAULT NULL,
  `kd4` int(11) UNSIGNED DEFAULT NULL,
  `kd5` int(11) UNSIGNED DEFAULT NULL,
  `kd6` int(10) UNSIGNED DEFAULT NULL,
  `kd7` int(10) UNSIGNED DEFAULT NULL,
  `kd8` int(10) UNSIGNED DEFAULT NULL,
  `uts` int(11) UNSIGNED DEFAULT NULL,
  `uas` int(11) UNSIGNED DEFAULT NULL,
  `kelas` int(11) NOT NULL,
  `guru_id` int(10) UNSIGNED NOT NULL,
  `semester` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `siswa_id` int(10) UNSIGNED NOT NULL,
  `mapel_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mapel_siswa`
--

INSERT INTO `mapel_siswa` (`id`, `kd1`, `kd2`, `kd3`, `kd4`, `kd5`, `kd6`, `kd7`, `kd8`, `uts`, `uas`, `kelas`, `guru_id`, `semester`, `siswa_id`, `mapel_id`, `created_at`, `updated_at`) VALUES
(1, 90, 100, 75, 77, 90, 60, 90, 78, 100, 100, 1, 10, 'semester 1', 6, 6, NULL, NULL),
(2, 90, 100, 77, 88, 90, 100, 70, 80, 90, 90, 1, 10, 'semester 1', 7, 6, NULL, NULL),
(3, 100, 90, 77, 54, 90, 100, 70, NULL, 90, 90, 1, 10, 'semester 1', 7, 7, NULL, NULL),
(4, 100, 100, 90, 90, 80, 90, 70, 100, 100, 100, 1, 10, 'semester 2', 7, 7, NULL, NULL),
(5, 100, 90, 77, 84, 60, 77, NULL, NULL, 90, 90, 1, 10, 'semester 1', 8, 8, NULL, NULL),
(6, 100, 100, 100, 90, 88, NULL, NULL, NULL, 90, 90, 1, 10, 'semester 2', 8, 7, NULL, NULL),
(7, 88, 90, 11, 80, 80, 77, 77, 90, 90, 90, 2, 10, 'semester 1', 8, 6, NULL, NULL),
(8, 100, 90, 100, 70, NULL, NULL, NULL, NULL, 90, 90, 1, 10, 'semester 1', 7, 7, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_03_17_154043_create_admins_table', 1),
(2, '2019_03_17_154534_create_guru_table', 1),
(3, '2019_03_19_025610_create_siswa_table', 2),
(4, '2019_03_19_032449_create_kelas_table', 3),
(5, '2019_03_19_033045_alter_siswa_table', 4),
(6, '2019_04_18_134230_create_orangtua_table', 5),
(7, '2019_04_23_061733_create_guru_table', 6),
(8, '2019_04_23_061823_create_orangtua_table', 6),
(9, '2019_04_23_061846_create_kelas_table', 6),
(10, '2019_04_25_130439_create_files_table', 7),
(11, '2019_05_26_131019_create_nilai_table', 8),
(12, '2019_05_26_131624_alter_nilai_table', 9),
(13, '2019_05_26_132421_create_mapel_table', 10);

-- --------------------------------------------------------

--
-- Table structure for table `orangtua`
--

CREATE TABLE `orangtua` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_belakang` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_kelamin` char(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` char(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orangtua`
--

INSERT INTO `orangtua` (`id`, `name`, `nama_belakang`, `email`, `alamat`, `jenis_kelamin`, `no_telp`, `password`, `created_at`, `updated_at`) VALUES
(25, 'muhammad', 'alwan arfianto', NULL, 'sleman', 'Pria', '08766555277', '$2y$10$djz4LLVJVeqTO6PzFZulpuWLmeuqaLoKw5bTNR5OFkh/PvSfbY/TW', '2019-08-24 20:56:28', '2019-08-24 20:56:28'),
(26, 'Agus', 'Joko Susilo', 'agusjokosusilo_@gmail.com', 'bantul', 'Pria', '085674568934', '$2y$10$zMH9ju2qI5G7.Nl8SqxNh.7dIM/NdzNMX9x1aZQgw26.xOOBF/KWy', '2019-08-24 23:48:38', '2019-08-24 23:48:38'),
(27, 'Roby', 'Sugara', NULL, 'bantul', 'Pria', '08122506545', '$2y$10$BubAsd7M2J5B3aTHPlUsneyq5ZDiFCTyS3lxsB3423oJK2yh7wDCu', '2019-08-24 23:51:16', '2019-08-24 23:51:16'),
(28, 'Puji', 'anto', NULL, 'bantul', 'Pria', '08122567656', '$2y$10$TerdhdhgeSEjRIzdXQm4AuuZQw0YFu5C6nVfQ4f2o6l6mBMO34lse', '2019-08-24 23:52:15', '2019-08-24 23:52:15'),
(29, 'Wisnu', 'Koentjoro', 'wisnu@gmail.com', 'bantul', 'Pria', '082250789645', '$2y$10$gKxrXnABno611Y.jls8kd.XFKuDV/jqMIM.tg1mM7wjZJo44DXSQ2', '2019-08-24 23:53:20', '2019-08-24 23:53:20'),
(30, 'Eko', 'Noviyanto', 'ekonoviyanto@gmail.com', 'bantul', 'Pria', '0856433118883', '$2y$10$dRGbf1DGiJ1iJ8p/2FdQPeBi63g8y2cMrRgQIZK5ZWrvVVA0MvrIu', '2019-08-24 23:54:09', '2019-08-24 23:54:09'),
(31, 'Munawir', 'Kholi', NULL, 'bantul', 'Pria', '0856778824567', '$2y$10$yYrDBgzUaa0i8qzgNng/7uEgo4mLCkd3a6R06kD3KYSfp2UcKgtea', '2019-08-24 23:54:53', '2019-08-24 23:54:53'),
(32, 'Putu', 'Wirna Jaya', NULL, 'bantul', 'Pria', '082225067885', '$2y$10$Evbo3US/U2lJAFRw/GGRx.iUzuVf.Dy685rxx5NfV6w38pWHH35ue', '2019-08-24 23:55:51', '2019-08-24 23:55:51'),
(33, 'Mutaqien', 'mukhlis', 'mukhlis3984@gmail.com', 'bantul', 'Pria', '081225089765', '$2y$10$VpXd4K4bHmqzz/WmowXNLuAngh7M0cB9Pk4ymrR.wX/OXb5cGOpjW', '2019-08-24 23:57:00', '2019-08-24 23:57:00'),
(34, 'Sutrisno', 'Juliawan', NULL, 'bantul', 'Pria', '087749675546', '$2y$10$V23xg.uf7c29S8XpsCiik.1o6ubPIM0c54/jlDHbs0FJHpl34uNpS', '2019-08-24 23:58:01', '2019-08-24 23:58:01'),
(35, 'Kholid', 'Prasetyo', 'kholidprasetyo45@gmail.com', 'bantul', 'Pria', '081225047658', '$2y$10$PKe.ODq/op.Wv43U7VzJqOXWCHBAA606DOa86mGNjjlW8lcyaoBLS', '2019-08-24 23:59:03', '2019-08-24 23:59:03'),
(36, 'Harry', 'Santoso', 'harrysantoso@gmail.com', 'bantul', 'Pria', '08774987654', '$2y$10$LdFUAs61uVFUjYOmP3u30ePjYjbGW7gEu0AGBXWWpjFTsd3WLLMpC', '2019-08-25 00:00:10', '2019-08-25 00:00:10'),
(37, 'Budi', 'Setiawan', NULL, 'bantul', 'Pria', '085643117645', '$2y$10$7sh6FWWKHKQU8SJB2QQCX.dpOPeodZDZeL.V2dwqFIAnBexzDCYQy', '2019-08-25 00:01:10', '2019-08-25 00:01:10'),
(38, 'Adhitya', 'Burhan Mustofa', 'adhitya54@gmail.com', 'bantul', 'Pria', '087456778977', '$2y$10$RcgCFPm8fZaJQX1b3VgC/.7IGnfsPvq87G7uZ131niI9v6QCoenNa', '2019-08-25 00:02:22', '2019-08-25 00:02:22'),
(39, 'Slamet', 'Sujoko', 'slametsujoko9@gmail.com', 'bantul', 'Pria', '08123667788', '$2y$10$LCIAbTeXNA6nMtl4QWcOW.XwIfSdy4mCJ8FkFfJ/CmMBbOBlcIgLq', '2019-08-25 00:03:22', '2019-08-25 00:03:22');

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `id` int(10) UNSIGNED NOT NULL,
  `nisn` char(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tempat_lahir` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `asal_sekolah` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_siswa` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_belakang` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_kelamin` char(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `orang_tua_id` int(10) UNSIGNED NOT NULL,
  `kelas_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`id`, `nisn`, `tempat_lahir`, `tanggal_lahir`, `asal_sekolah`, `nama_siswa`, `nama_belakang`, `alamat`, `jenis_kelamin`, `orang_tua_id`, `kelas_id`, `created_at`, `updated_at`) VALUES
(6, '5678', 'bantul', '2016-03-31', 'Tk Kuncup Harapan', 'Alifiya', 'Permata Sari', 'bantul', 'Wanita', 25, 2, '2019-08-25 00:08:54', '2019-09-05 18:56:14'),
(7, '5679', 'bantul', '2014-03-21', 'playgroup sekar arum', 'Apprillia', 'Manda Islami', 'bantul', 'Wanita', 26, 1, '2019-08-25 07:19:09', '2019-08-25 07:19:09'),
(8, '5680', 'bantul', '2014-04-30', 'Tk Nurul Huda', 'Beni', 'Setiawan', 'bantul', 'Pria', 27, 2, '2019-08-25 07:20:39', '2019-08-25 09:17:45'),
(9, '5681', 'bantul', '2014-03-29', 'tk nurul huda', 'Ceria', 'Kidung Saraswati', 'bantul', 'Wanita', 29, 1, '2019-08-25 07:21:45', '2019-08-25 07:21:45'),
(10, '5682', 'bantul', '2014-02-24', 'playground sekar arum', 'Dina', 'Laura Syahrini', 'bantul', 'Wanita', 28, 1, '2019-08-25 07:23:01', '2019-08-25 07:23:01'),
(11, '5683', 'bantul', '2014-02-12', 'Tk Pertiwi patalan', 'Dwi', 'Joko Novianto', 'bantul', 'Pria', 31, 2, '2019-08-25 07:25:23', '2019-08-25 09:52:28'),
(12, '5684', 'bantul', '2014-01-03', 'Tk Pertiwi Patalan', 'Faisal', 'Priyadi', 'bantul', 'Pria', 33, 1, '2019-08-25 07:26:30', '2019-08-25 07:26:30'),
(13, '5685', 'bantul', '2014-02-02', 'Tk Pertiwi Patalan', 'Febriana', 'Dela Putri', 'bantul', 'Wanita', 37, 1, '2019-08-25 07:27:15', '2019-08-25 07:27:15'),
(14, '5686', 'bantul', '2014-12-09', 'TK ABA Tonayan', 'Hani', 'Agustina Dwi Ambara', 'bantul', 'Wanita', 39, 1, '2019-08-25 07:28:46', '2019-08-25 07:28:46'),
(15, '5687', 'bantul', '2014-04-02', 'TK Nurul Huda', 'Irfansyah', 'Praramadhan', 'bantul', 'Pria', 34, 1, '2019-08-25 07:30:12', '2019-08-25 07:30:12'),
(16, '5688', 'bantul', '2014-05-05', 'TK Dharmarena', 'Jean', 'Cristabel Aurellia', 'bantul', 'Wanita', 35, 1, '2019-08-25 07:33:15', '2019-08-25 07:33:15'),
(17, '5889', 'bantul', '2014-05-02', 'Tk Mahardika', 'Kharisma', 'Meylisa Azzahra', 'bantul', 'Wanita', 36, 1, '2019-08-25 07:43:14', '2019-08-25 07:43:14'),
(18, '5890', 'bantul', '2014-07-04', 'TK Mahardika', 'Khoirun', 'Nissa Azzahra', 'bantul', 'Wanita', 37, 1, '2019-08-25 07:46:52', '2019-08-25 07:46:52'),
(19, '5691', 'bantul', '2014-08-02', 'Tk Aba Imogiri', 'Linda', 'Rahmawati', 'bantul', 'Wanita', 38, 1, '2019-08-25 07:48:40', '2019-08-25 07:48:40'),
(20, '5892', 'bantul', '2014-08-08', 'TK ABA Imogori', 'Linda', 'Rahmawati', 'bantul', 'Wanita', 33, 1, '2019-08-25 07:53:31', '2019-08-25 07:53:31'),
(21, '5693', 'bantul', '2014-03-03', 'Tk Tridaya Canden', 'Raihan', 'Fadli Kurniawan', 'bantul', 'Pria', 35, 1, '2019-08-25 07:54:53', '2019-08-25 07:54:53'),
(22, '5694', 'bantul', '2014-09-04', 'Tk Tridaya Canden', 'Rizal', 'Ahmad', 'bantul', 'Pria', 32, 1, '2019-08-25 07:56:48', '2019-08-25 07:56:48'),
(23, '5695', 'bantul', '2014-05-05', 'Tk Tridaya Canden', 'Rizal', 'Ahmad Fauzi', 'bantul', 'Pria', 31, 1, '2019-08-25 07:57:46', '2019-08-25 07:57:46'),
(24, '5696', 'bantul', '2014-09-09', 'Tk Kuncup Harapan', 'Rizki', 'Teguh Narimo', 'bantul', 'Pria', 30, 1, '2019-08-25 07:59:43', '2019-08-25 07:59:43'),
(25, '5697', 'Bantul', '2014-06-05', 'Tk Kuncup Harapan', 'Roland', 'Ade Zulfan', 'Bantul', 'Pria', 37, 1, '2019-08-25 08:01:36', '2019-08-25 08:01:36'),
(26, '5698', 'Bantul', '2014-07-30', 'TK Nurul Huda', 'Sammy', 'Abiyyu', 'bantul', 'Pria', 30, 1, '2019-08-25 08:09:39', '2019-08-25 08:09:39'),
(27, '5699', 'Bantul', '2014-12-13', 'Tk Kuncup Harapan', 'Syella', 'Indah Sari', 'Bantul', 'Wanita', 35, 1, '2019-08-25 08:14:18', '2019-08-25 08:14:18'),
(28, '5700', 'Bantul', '2014-08-08', 'Tk Tridaya Canden', 'Yanuarti', 'Eka Ratri', 'bantul', 'Pria', 26, 1, '2019-08-25 08:17:18', '2019-08-25 08:17:18'),
(29, '5701', 'Bantul', '2014-04-06', 'Tk Kuncup Harapan', 'Zulfa', 'Hamidah', 'Bantul', 'Wanita', 30, 1, '2019-08-25 08:18:49', '2019-08-25 08:18:49'),
(30, '5702', 'Bantul', '2014-04-19', 'Tk Pembina Bantul', 'Hamdan', 'Ramdhani', 'Bantul', 'Pria', 39, 2, '2019-08-25 08:19:53', '2019-08-25 08:23:08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `guru_id` (`guru_id`);

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mapel`
--
ALTER TABLE `mapel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mapel_siswa`
--
ALTER TABLE `mapel_siswa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `siswa_id` (`siswa_id`),
  ADD KEY `mapel_id` (`mapel_id`),
  ADD KEY `guru_id` (`guru_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orangtua`
--
ALTER TABLE `orangtua`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kelas_id` (`kelas_id`),
  ADD KEY `orangtua_id` (`orang_tua_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `guru`
--
ALTER TABLE `guru`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `mapel`
--
ALTER TABLE `mapel`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `mapel_siswa`
--
ALTER TABLE `mapel_siswa`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `orangtua`
--
ALTER TABLE `orangtua`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `files`
--
ALTER TABLE `files`
  ADD CONSTRAINT `files_ibfk_1` FOREIGN KEY (`guru_id`) REFERENCES `guru` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `mapel_siswa`
--
ALTER TABLE `mapel_siswa`
  ADD CONSTRAINT `mapel_siswa_ibfk_2` FOREIGN KEY (`mapel_id`) REFERENCES `mapel` (`id`),
  ADD CONSTRAINT `mapel_siswa_ibfk_3` FOREIGN KEY (`guru_id`) REFERENCES `guru` (`id`),
  ADD CONSTRAINT `mapel_siswa_ibfk_4` FOREIGN KEY (`siswa_id`) REFERENCES `siswa` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `siswa`
--
ALTER TABLE `siswa`
  ADD CONSTRAINT `siswa_ibfk_1` FOREIGN KEY (`kelas_id`) REFERENCES `kelas` (`id`),
  ADD CONSTRAINT `siswa_ibfk_2` FOREIGN KEY (`orang_tua_id`) REFERENCES `orangtua` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
